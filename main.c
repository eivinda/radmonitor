#include <stdio.h>
#include "radmon.h"

int
main( int argc, char **argv )
{
	char   buff[128];
	size_t len;
	radmon_init();
	while( 1 ) {
		len = radmon_read( buff, sizeof buff );
		printf( "%s", buff );
	}

	return 0;
}
