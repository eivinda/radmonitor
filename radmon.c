#include <stdio.h>
#include "radmon.h"

static volatile uint32_t *SPI0_CONTROL      = (uint32_t*)0x40001000;
static volatile uint32_t *SPI0_TXRXDF_SIZE  = (uint32_t*)0x40001004;
static volatile uint32_t *SPI0_RX           = (uint32_t*)0x40001010;
static volatile uint32_t *SPI0_TX           = (uint32_t*)0x40001014;
static volatile uint32_t *SPI0_CLK_GEN      = (uint32_t*)0x40001018;
static volatile uint32_t *SPI0_SLAVE_SELECT = (uint32_t*)0x4000101c;

static inline void
spi0_tx( uint8_t addr, uint16_t data )
{
	uint32_t command;
	command = 0 << 24 | addr << 16 | data;
	*SPI0_TX = command;
}


static inline uint16_t
spi0_rx( uint8_t addr )
{
	uint32_t data;
	*SPI0_TX = 1 << 24 | addr << 16;
	data = *SPI0_RX;
	return data & 0xffff;
}


void
radmon_init(void)
{
	*SPI0_CONTROL = 0x90000103;
	*SPI0_TXRXDF_SIZE = 32;
	*SPI0_CLK_GEN = 0x13;
	*SPI0_SLAVE_SELECT = 0x2;
}


size_t
radmon_read( char *buff, size_t n )
{
	uint16_t status = spi0_rx( 0x02 );
	uint16_t SEU_c1 = spi0_rx( 0x06 );
	uint16_t MBU_c1 = spi0_rx( 0x07 );
	uint16_t SEU_c2 = spi0_rx( 0x08 );
	uint16_t MBU_c2 = spi0_rx( 0x09 );
	uint16_t SEU_c3 = spi0_rx( 0x0a );
	uint16_t MBU_c3 = spi0_rx( 0x0b );
	uint16_t SEU_c4 = spi0_rx( 0x0c );
	uint16_t MBU_c4 = spi0_rx( 0x0d );
	uint16_t curr   = spi0_rx( 0x10 );
	uint16_t temp   = spi0_rx( 0x12 );
	uint16_t vcc1   = spi0_rx( 0x13 );
	uint16_t vcc2   = spi0_rx( 0x14 );
	return snprintf( buff, n,
			"RM 0x%x; 0x%x; 0x%x; 0x%x; 0x%x; 0x%x; 0x%x; 0x%x; 0x%x; 0x%x; 0x%x; 0x%x; 0x%x\n",
			status, SEU_c1, MBU_c1, SEU_c2, MBU_c2, SEU_c3, MBU_c3,
			SEU_c4, MBU_c4, curr, temp, vcc1, vcc2 );
}
