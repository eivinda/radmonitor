#!/bin/sh

# SPI 0 control registers
SPI0_CONTROL="0x40001000"
SPI0_TXRXDF_SIZE="0x40001004"
SPI0_RX="0x40001010"
SPI0_TX="0x40001014"
SPI0_CLK_GEN="0x40001018"
SPI0_SLAVE_SELECT="0x4000101c"

# Sends data to RadMON
# Usage: tx addr [data]
# where addr is a 2-digit hexadecimal number
# and data is a optional 4-digit hexadecimal number
tx()
{
	local addr=$1
	local data=$2
	if [ -z "$data" ]; then
		data="0000"
	fi
	peekpoke 32 $SPI0_TX "0x01${addr}${data}" > /dev/null
}

# Reads data from RadMON
# Usage: rx addr
# where addr is a 2-digit hexadecimal number
rx()
{
	local addr=$1
	peekpoke 32 $SPI0_TX "0x00${addr}0000" > /dev/null
	peekpoke 32 $SPI0_RX
}

# Sets SPI 0 and RadMON to correct state.
init()
{
	# Set SPI 0 cotrol register.
	peekpoke 32 $SPI0_CONTROL 0x90000103 > /dev/null
	# Set tx/rx size to 32
	peekpoke 32 $SPI0_TXRXDF_SIZE 0x20 > /dev/null
	# Set clock gen to 2.5MHz
	peekpoke 32 $SPI0_CLK_GEN 0x13 > /dev/null
	# Set slave select to 2
	peekpoke 32 $SPI0_SLAVE_SELECT 0x02 > /dev/null
	# Enable RadMON
	tx 00 0133
	# Clear counters
	tx 01
	# Read status and RadMON version
	local status=$(rx 02)
	local ver=$(rx 05)
	echo "RadMON status $status version ${ver}\n"
}

# Prints registers read from RadMON
dump_regs()
{
	local status=$(rx 02)
	local SEU_c1=$(rx 06)
	local MBU_c1=$(rx 07)
	local SEU_c2=$(rx 08)
	local MBU_c2=$(rx 09)
	local SEU_c3=$(rx 0a)
	local MBU_c3=$(rx 0b)
	local SEU_c4=$(rx 0c)
	local MBU_c4=$(rx 0d)

	echo "${status}:${SEU_c1}:${MBU_c1}:${SEU_c2}:${MBU_c2}:${SEU_c3}:${MBU_c3}:${SEU_c4}:${MBU_c4}"
}

init

while [ -n "true" ]; do
	dump_regs
	sleep 1
done
