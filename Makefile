SRC    := radmon.c main.c
OBJS   := $(patsubst %.c,%.o,${SRC})
TARGET := radmonitor

CC     := ${CROSS_COMPILE}gcc

all : ${TARGET}

clean :
	rm *.o ${TARGET} ${TARGET}.gdb || true

.PHONY : all clean

${TARGET} : ${OBJS}
	${CC} ${LDFLAGS} ${OBJS} -o ${TARGET}

main.o : main.c radmon.h

radmon.o : radmon.c radmon.h
